% Project ESS for the Meta  
% Creator: Adam S
% Date 8/16/2022

clc; clear;
tic;

format long

%Number of decks here
%prompt = "What is the number of decks?: ";
%deckNum = input(prompt);
deckNum = 16;
deckMax = deckNum;

doneDecks = [];



%Minimum winrate (user-picked)
%prompt = "What is the minimum winrate% (49% Recommended): ? ";
%minWin = input(prompt);
minWin = 0.5;

%Where data is located
T = readtable('data/matchup10dash3dash7.csv');
winrateMat = table2array(T);


%Minimum change to winrate to escape algorithm(COMMENTED OUT)
%minChange = .05;

%Number of generations to complete
genZ = 1000;

%Provided Freq Matrix here
providedFreqMat = readtable('data/freq10dash3dash7.csv');


%Starting frequency matrix
freqMat = zeros(deckNum,genZ+1);
for i = 1:deckNum
    freqMat(i, 1) = 1/deckNum;
end

%Winrate Matrix (for Debugging)
winrateMatty = zeros(deckNum, genZ+1);
changeFacMat = zeros(deckNum, genZ+1);
deltaMat = zeros(deckNum, genZ +1);

generations = 2;
%Start Evolutionary Algorithm
while generations < (genZ+1)
    for j = 1:deckNum
        %Find the average winrate of each deck
        winrateTemp = 0;
        if (freqMat(j, generations-1) == 0)
            freqMat(j, generations) = 0;
            winrateMatty(j, generations) = 2;
            changeFacMat(j, generations) = 2;
            deltaMat(j, generations) = 2;
            continue;
        end
        for k = 1:deckNum
            winrateTemp = winrateTemp + (freqMat(k, generations-1) * winrateMat(j,k));
        end
        winrateMatty(j, generations) = winrateTemp;
        %If less than minWin, change frequency, add to changeSum
        if winrateTemp < minWin
            relErr = (minWin - winrateTemp)/minWin;
            changeFac = 1 - relErr;
            changeFacMat(j, generations) = changeFac;
            freqMat(j, generations) = freqMat(j, generations-1) * changeFac;
            %delta = (winrateTemp - minWin) * freqMat(j, generations -1) * (genZ + 1 - generations)/(genZ + 1);
            %deltaMat(j, generations) = delta;
            %freqMat(j, generations) = freqMat(j, generations -1) - delta;
        end
        if winrateTemp >= minWin
            freqMat(j, generations) = freqMat(j, generations-1);
            changeFacMat(j, generations) = 1;
            deltaMat(j, generations) = 0;
        end
    end
    freqMat(:, generations) = freqMat(:, generations) / sum(freqMat(:, generations));
    %If a deck has a frequency less than 0.02, cut it
    l = 1;
    deckLoss = 0.001;
    while l <= deckNum
        if freqMat(l, generations) < deckLoss
            %freqMat(l, generations) = [];
            %winrateMat(l,:) = [];
            %winrateMat(:,l) = [];
            %deckNum = deckNum - 1;
            %l = l - 1;
            freqMat(l, generations) = 0;

        end
        l = l + 1;
    end
    freqMat(:, generations) = freqMat(:, generations) / sum(freqMat(:, generations));
    %If the freq matrix hasn't changed much, end algorithm(COMMENTED OUT)
    %if changeSum < minChange
        %break;
    %end
    generations = generations + 1;
end
%Plot vector
x = 1:(genZ + 1);
hold on
%CumulSum is cumulative sum of freqMat
cumulSum = freqMat;
%Generate random color for each deck
colors = rand(deckMax, 3);
%Put correct values in cumulSum
for j = 1:deckMax
    if j ~= deckMax
        cumulSum(j+1, :) = cumulSum(j+1, :) + cumulSum(j, :);
    end
    plot(x,cumulSum(j, :), 'color', colors(j, :));
end
%Set colors for the area
colororder(colors)
%Plot area under graph in reverse order
for j = 1:deckMax
    k = (deckMax + 1) - j;
    area(x,cumulSum(k,:))
end
legendNames = ["Beast Hunter", "Relic DemonHunter", "Ramp Druid", "Imp Warlock", "Aggro DemonHunter", "Secret Mage", "Control Paladin", "Pure Paladin", "Thief Priest", "Thief Rogue", "Control Shaman", "Spooky Mage", "Aggro Druid", "Quest Priest", "Bless Priest", "Control Warrior"];
legend({'Beast Hunter', 'Relic DemonHunter', 'Ramp Druid', 'Imp Warlock', 'Aggro DemonHunter', 'Secret Mage', 'Control Paladin', 'Pure Paladin', 'Thief Priest', 'Thief Rogue', 'Control Shaman', 'Spooky Mage', 'Aggro Druid', 'Quest Priest', 'Bless Priest', 'Control Warrior'})
outOne = 'Evolutionary Algorithm Results:\n\n';
k = find(freqMat(:,genZ));
fprintf(outOne);
for j = 1:length(k)
    name = legendNames(k(j));
    formatSpec = 'Surviving Deck: %s   Playrate: %5.3f %%\n';
    fprintf(formatSpec,name, freqMat(k(j), genZ));
end
fprintf('\n');
toc
