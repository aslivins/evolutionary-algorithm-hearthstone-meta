# Evolutionary Algorithm Hearthstone Meta



## Project Goals

1) Gather data from ViciousSyndicate's VS Gold Live App for Deck Matchup Winrates and play rates by webscraping

2) Format an Evolutionary Algorithm to the winrate data, and iterate over N generations to find optimal mixed strategy for Ranked play. 

Deliverable: Optimal mixed strategy that gives one the best chance of winning on Ranked play given winrate data.

Report still needs to be written.
